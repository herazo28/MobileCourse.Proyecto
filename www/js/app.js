// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

// Variable que representa la base de datos
//var db = null;

var WeRunners = angular.module('WeRunners', ['ionic', 'ngCordova', 'ogUtils','firebase']);

WeRunners.run(function($ionicPlatform, $cordovaSQLite, $rootScope,$firebaseArray,Items, Sesion) {
  $ionicPlatform.ready(function() {
    console.log("STARTED");
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    // Android customization
    // To indicate that the app is executing tasks in background and being paused would disrupt the user.
    // The plug-in has to create a notification while in background - like a download progress bar.
    cordova.plugins.backgroundMode.setDefaults({
        title:  'WeRunners',
        text:   'Corriendo.'
    });
    // Enable background mode
    cordova.plugins.backgroundMode.enable();

    // Called when background mode has been activated
    cordova.plugins.backgroundMode.onactivate = function () {

    }

      if(window.cordova) {
          db = $cordovaSQLite.openDB({name: "WeRunnersDB.db", location: 1});
      } else {
          db = window.openDatabase("WeRunnersDB.db", "1.0", "WeRunners", -1);
      }
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS cancion (ruta text primary key, bpm int)");

      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS sesion (id text primary key, fechaInicio text, fechaFin text, nombre text, descripcion text, key text, phone text)");

      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS reproduccion (id text , cancion text, fecha text)");


      $rootScope.telefono = window.localStorage['telefono'] || 0;
      if($rootScope.telefono == 0){
          window.plugins.phonenumber.get(success, failed);

          function success(phonenumber) {
              while(phonenumber != null && phonenumber.length != 10){
                phonenumber  = prompt("Cual es su numero celular? (10 digitos)", phonenumber);
              }
                phonenumber = phonenumber != null ? phonenumber : "";
                window.localStorage['telefono'] = phonenumber;

                $rootScope.firebaseUsersRef = new Firebase("https://werunners.firebaseio.com/users/");
                $rootScope.firebaseUsers = $firebaseArray($rootScope.firebaseUsersRef);
                $rootScope.firebaseUsers.$add({
                  "phone": phonenumber
                }).then(function(ref) {
                    window.localStorage['id'] = ref.key();
                  //TODO crear el nuevo usuario en el SQLite de este telefono
                  $rootScope.user = {};
                  $rootScope.user.userId = ref.key();
                  $rootScope.user.phone = phonenumber;
                  $rootScope.user.items = Items.run();
                });
          }
          function failed(msg) {
              console.log("Error " + msg);
              success('');
          }
      }
      else{
          $rootScope.user = {};
          $rootScope.user.userId =    window.localStorage['id'];
          $rootScope.user.phone =  window.localStorage['telefono'];
          if(/*conexion*/1){
              $rootScope.user.items = Items.run();
          }


          $rootScope.firebaseUsersRef = new Firebase("https://werunners.firebaseio.com/users/");
          $rootScope.firebaseUsers = $firebaseArray($rootScope.firebaseUsersRef);

          // items
      }
  });
})

WeRunners.controller('homeCtrl', function ($scope, $firebaseArray, $cordovaDeviceMotion, $ionicPlatform, $rootScope, BPMRun, Items, $ionicModal, $state, Sesion, BPMRun, $ionicPopup, Reproduccion, $cordovaGeolocation) {

    $scope.items = Items;
  $rootScope.user = $rootScope.user != undefined ? $rootScope.user : {};
  $scope.items = $rootScope.user.items;
    $scope.data = {};

    $scope.latitude = 0
    $scope.longitude = 0

    
    

    // watch Acceleration options
   options = {
        frequency: 100, // Measure every 100ms
        deviation : 25  // We'll use deviation to determine the shake event, best values in the range between 25 and 30
    };

    // Current measurements
    $scope.measurements = {
        x : null,
        y : null,
        z : null,
        timestamp : null
    }

    // Watcher objec
    $scope.watch = null;
    $rootScope.idSesion = null

    $scope.btn_iniciar = "Iniciar";

    $scope.sesion = {
        id: $rootScope.idSesion,
        fechaInicio: null,
        fechaFin: null,
        nombre: null,
        descripcion: null,
        canciones: [],
        phone: $rootScope.user.phone
    }

    $scope.canciones = [];
   // Start measurements when Cordova device is ready
    $ionicPlatform.ready(function() {
            
        
        

        var posOptions = {timeout: 10000, enableHighAccuracy: true};
  $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
      $scope.latitude  = position.coords.latitude
      $scope.longitude = position.coords.longitude
    }, function(err) {
      alert('code1: '    + err.code    + '\n' +
            'message: ' + err.message + '\n');
    });



    // begin watching
    /*
    var watch = $cordovaGeolocation.watchPosition({ frequency: 1000 });
    watch.promise.then(function() { /* Not  used  },
        function(err) {
            // An error occurred.
            alert('code: '    + err.code    + '\n' +
                'message: ' + err.message + '\n');
        },
        function(position) {
            $scope.latitude  = position.coords.latitude
            $scope.longitude = position.coords.longitude
        });
    // clear watch
    $cordovaGeolocation.clearWatch(watch.watchID)
    */




        $scope.iniciarParar = function() {

            if ('null' == JSON.stringify($scope.watch)) {
                crear = Sesion.crear($rootScope.user.phone)
                crear.then(function(result) {
                    $rootScope.idSesion = result;

                });
                $scope.btn_iniciar = "Parar";
                // Device motion configuration
                $scope.watch = $cordovaDeviceMotion.watchAcceleration(options);

                // Device motion initilaization
                $scope.watch.then(null, function(error) {
                    console.log('Error');
                },function(result) {
                  /*
                  cordova.plugins.backgroundMode.configure({
                    ticker: ""+result.x
                  })*/
                    // Set current data
                    $scope.measurements.x = result.x;
                    $scope.measurements.y = result.y;
                    $scope.measurements.z = result.z;
                    $scope.measurements.timestamp = result.timestamp;

                    BPMRun.registrar($scope.measurements);

                    });
            } else {
                if ('null' != JSON.stringify($scope.watch)) {
                    $scope.watch.clearWatch(); // Turn off motion detection watcher
                }
                $scope.watch = null;
                $scope.btn_iniciar = "Iniciar";
                Sesion.cerrar($rootScope.idSesion);
                BPMRun.terminarSesion();

                var popup = $ionicPopup.show({
                    template: 'nombre:<input type="text" ng-model="data.nombre">descripción:<input type="text" ng-model="data.descripcion">',
                    title: 'Por favor, ingrese un nombre y descripción para la sesión que acaba de terminar',
                    scope: $scope,
                    buttons: [    
                      {
                        text: '<b>Guardar</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                          if (!$scope.data.nombre && $scope.data.descripcion) {
                            //don't allow the user to close unless he enters wifi password
                            e.preventDefault();
                          } else {
                            return $scope.data.wifi;
                          }
                        }
                      }
                    ]
                  });

                popup.then(function(res) {
                    Sesion.guardar($rootScope.idSesion, $scope.data.nombre, $scope.data.descripcion);
                    var obtenerFechas = Sesion.get($rootScope.idSesion).then(function(result) {
                        for(var i = 0; i < result.length; i ++) {
                           $scope.data.fechaInicio = result[i].fechaInicio
                           $scope.data.fechaFin = result[i].fechaFin
                        }

                        Reproduccion.getById($rootScope.idSesion).then(function(result) {
                        for(var i = 0; i < result.length; i ++) {
                            temp = result[i].cancion.split("/");
                            nombre = temp[temp.length - 1]
                            nombre = nombre.replace(/%20/g, " ");
                            nombre = nombre.replace(/%C3%B3/g, "ó");
                            nombre = nombre.replace(/%C3%AD/g, "í");
                            nombre = nombre.replace(/%C3%A1/g, "á");
                            $scope.canciones.push({nombre: nombre, fecha: result[i].fecha});

                            }
                        })

                    });

                    obtenerFechas.then(function(res) {


                        phonenumber = $rootScope.user.phone
                        for(var i =0;phonenumber != null && i<$rootScope.firebaseUsers.length;i++){
                              if(phonenumber == $rootScope.firebaseUsers[i].phone){
                                  $scope.otherUserItemsRef = $rootScope.firebaseUsersRef.child($rootScope.firebaseUsers[i].$id+"/items");
                                  var otherUserItems = $firebaseArray($scope.otherUserItemsRef);
                                  otherUserItems.$add({
                                       "id": $rootScope.idSesion,
                                       "fechaInicio": $scope.data.fechaInicio,
                                       "fechaFin": $scope.data.fechaFin,
                                       "nombre": $scope.data.nombre,
                                       "descripcion": $scope.data.descripcion,
                                       "phone": $rootScope.user.phone
                                  }).then(function(ref) {
                                      var cancionesArray = $firebaseArray($scope.otherUserItemsRef.child(ref.key() + "/canciones"));
                                      for(var j = 0; j < $scope.canciones.length; j ++) {
                                          cancionesArray.$add({
                                          "cancion": $scope.canciones[j].nombre
                                      })
                                      }

                                  });
                              }
                        }

                    })

                    $state.go('detalleSesion')
                })


                //$state.go('detalleSesion')
            }
        }
    });



    // TODO: Pasar
    $scope.saveEvent = function(){
      //prueba de enviar datos al backend
      /*$rootScope.user.items.$add({
        "name": "Me",
        "phone": $rootScope.user.phone,
        "Accelerometer": $scope.measurements.x
      });*/

      var phonenumber = '';
      while(phonenumber != null && phonenumber.length != 10){
        phonenumber  = prompt("Quieres compartir tu evento? (10 digitos)", phonenumber);
      }
      for(var i =0;phonenumber != null && i<$rootScope.firebaseUsers.length;i++){
          if(phonenumber == $rootScope.firebaseUsers[i].phone){
              var otherUserItems = $firebaseArray($rootScope.firebaseUsersRef.child($rootScope.firebaseUsers[i].$id+"/items"));
              otherUserItems.$add({
                "name": $rootScope.user.phone,
                "phone": $rootScope.user.phone,
                "Accelerometer": $scope.measurements.x
              });
          }
      }
      /*
      .then(function(ref) {
        var id = ref.key();
        console.log("added record with id " + id);

      });*/
    };

    $scope.$on('$ionicView.beforeLeave', function(){
        if ('null' != JSON.stringify($scope.watch)) {
            $scope.watch.clearWatch(); // Turn off motion detection watcher
        }
        $scope.watch = null;
    });
});


WeRunners.controller('eventosCtrl', function ($scope, $ionicHistory, $rootScope, Items) {
    $rootScope.user = $rootScope.user != undefined ? $rootScope.user : {};
    $scope.items = $rootScope.user.items;
    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };
});

WeRunners.controller('configurationCtrl', function ($scope, $ionicHistory, $rootScope) {
    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };



    $rootScope.sensibilidad = window.localStorage['sensibilidad'] || 70;
    $rootScope.busquedaCanciones = window.localStorage['busquedaCanciones'] || 'Nunca';
    $rootScope.multiplicacion = window.localStorage['multiplicacion'] || 1

    $scope.configs = [{id: 0, name: 'Sensibilidad del acelerómetro', value: $rootScope.sensibilidad, namepro: 'sensibilidad'}, {id: 1, name: 'Buscar canciones', value: $rootScope.busquedaCanciones, namepro: 'busquedaCanciones'}, {id: 2, name: 'Factor de multiplicación', value: $rootScope.multiplicacion, namepro: 'factorMultiplicacion'}]

});

WeRunners.controller('sensibilidadCtrl', function ($scope, $cordovaPreferences, $ionicHistory, $rootScope) {

    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };

    $scope.vlr_sensibilidad = {
        valor: $rootScope.sensibilidad
    };

    $scope.guardarSensibilidad = function() {
        window.localStorage['sensibilidad'] = $scope.vlr_sensibilidad.valor;
        $rootScope.sensibilidad = window.localStorage['sensibilidad'] || 0;
        alert("La sensibilidad fue puesta en " + $scope.vlr_sensibilidad.valor);
        $ionicHistory.goBack();
    }
});

WeRunners.controller('multiplicacionCtrl', function ($scope, $cordovaPreferences, $ionicHistory) {

    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };

    $scope.vlr_multiplicacion = {
        valor: window.localStorage['multiplicacion'] || 1
    };

    $scope.guardarMultiplicacion = function() {
        window.localStorage['multiplicacion'] = $scope.vlr_multiplicacion.valor;
        alert("El factor de multiplicación fue puesto en " + $scope.vlr_multiplicacion.valor);
        $ionicHistory.goBack();
    }


});

WeRunners.controller('busquedaCancionsCtrl', function ($scope, $cordovaPreferences, $ionicHistory, $ionicPlatform, $fileFactory, Cancion) {

    // Función para navegación
    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };

    $scope.files = []

    // Función para búsqueda de archivos *.mp3

    directorios = []
    temp = []
    canciones = []

    // Inicialización de la  fileFactory
    var fs = new $fileFactory();

    $scope.buscarCanciones = function() {

        // Se obtienen los archivos y se colocan en la variable files
        fs.getEntriesAtRoot().then(function(result) {

            numero = 0;

            Cancion.removeAll();

            // Se agregan las carpetas de la raiz
            for (i = 0; i < result.length; i++) {
                if(result[i].isDirectory){
                    directorios.push(result[i])
                } else {
                    if(endsWith(result[i].nativeURL, '.mp3')) {
                        canciones.push(result[i])
                    }
                }
            }

            while (directorios.length > 0) {
                fs.getEntries(directorios.pop().nativeURL).then(function(result) {
                    for (i = 0; i < result.length; i++) {
                        if(result[i].isDirectory){
                            directorios.push(result[i]);
                        } else {
                            if(endsWith(result[i].nativeURL, '.mp3')) {
                                canciones.push(result[i]);
                                $scope.files.push(result[i]);

                                // Aquí va código para guardar la cancion en la base de datos
                                $scope.currentbpm = Math.floor((Math.random() * 300) + 1);
                                pcancion = {ruta: result[i].nativeURL, bpm: $scope.currentbpm}
                                Cancion.add(pcancion);
                                numero =  numero + 1
                            }
                        }
                    }

                });
            }
            actualizarFecha();
            $ionicHistory.goBack();

        }, function(error) {
            alert("1");
            alert(error);
        });
    };

    // Función que permite saber si una cadena termina con uns subcadena determinada
    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }


    $scope.vlr_busquedaCanciones = {
        valor: window.localStorage['busquedaCanciones'] || 'Nunca'
    };

    function actualizarFecha() {
        var f = new Date();
        window.localStorage['busquedaCanciones'] = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        alert("La busqueda de canciones en el dispositivo ha terminado");
    }

});

WeRunners.controller('reproductorCtrl', function ($scope, $ionicHistory, Cancion, $cordovaMedia) {

    var media = null;

    $scope.canciones = []


    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };

    Cancion.all().then(function(team){
        for (i = 0; i < team.length; i++) {
                temp = team[i].ruta.split("/");
                nombre = temp[temp.length - 1]
                nombre = nombre.replace(/%20/g, " ");
                nombre = nombre.replace(/%C3%B3/g, "ó");
                nombre = nombre.replace(/%C3%AD/g, "í");
                nombre = nombre.replace(/%C3%A1/g, "á");

                $scope.canciones.push({"nombre": nombre, "bpm": team[i].bpm, "ruta": team[i].ruta})

            }
    });

    $scope.playfromsong = function(cancion) {
        if ('null' != JSON.stringify(media)) {
            media.stop();
        }
        media = $cordovaMedia.newMedia(cancion.ruta);
        media.play(); // Android
        document.getElementById("playpause").className = "button icon ion-pause";
    }

    $scope.stop = function() {
        if ('null' != JSON.stringify(media)) {
            media.stop();
        }
        media = null;
    }

    $scope.play = function() {
        if ('null' != JSON.stringify(media)) {
            if (document.getElementById("playpause").className == "button icon ion-pause" || document.getElementById("playpause").className == "button icon ion-pause activated") {
                media.pause();
                document.getElementById("playpause").className = "button icon ion-play"
            } else if(document.getElementById("playpause").className == "button icon ion-play" || document.getElementById("playpause").className == "button icon ion-play activated") {
                media.play();
                document.getElementById("playpause").className = "button icon ion-pause"
            }
        }
    }


    $scope.previous = function() {
        if ('null' != JSON.stringify(media)) {
            encontrado = false;
            for (i = 0; i < $scope.canciones.length && !encontrado; i++) {
                if ($scope.canciones[i].ruta == media.media.src && i != 0) {
                    media.stop();
                    media = $cordovaMedia.newMedia($scope.canciones[i - 1].ruta);
                    media.play(); // Android
                    encontrado = true;
                }
            }
        }
    }

    $scope.next = function() {
        if ('null' != JSON.stringify(media)) {
            encontrado = false;
            for (i = 0; i < $scope.canciones.length && !encontrado; i++) {
                if ($scope.canciones[i].ruta == media.media.src && i != $scope.canciones.length - 1) {
                    media.stop();
                    media = $cordovaMedia.newMedia($scope.canciones[i + 1].ruta);
                    media.play(); // Android
                    encontrado = true;
                }
            }
        }
    }
});

WeRunners.controller('detalleSesionCtrl', function ($scope, $ionicHistory, Sesion, $rootScope, $ionicPlatform, Reproduccion, $firebaseArray) {
    $scope.sesion = {
        id: $rootScope.idSesion,
        fechaInicio: null,
        fechaFin: null,
        nombre: null,
        descripcion: null,
        canciones: [],
        phone: $rootScope.user.phone
    }

    $scope.canciones = []

    Sesion.getSesion($scope.sesion.id).then(function(res) {
        for(var i = 0; i < res.length; i ++) {
            $scope.sesion.fechaInicio = res[i].fechaInicio;
            $scope.sesion.fechaFin = res[i].fechaFin;
            $scope.sesion.nombre = res[i].nombre;
            $scope.sesion.descripcion = res[i].descripcion;
        }
    });

    Reproduccion.getById($scope.sesion.id).then(function(res) {
        for(var i = 0; i < res.length; i ++) {
            temp = res[i].cancion.split("/");
            nombre = temp[temp.length - 1]
            nombre = nombre.replace(/%20/g, " ");
            nombre = nombre.replace(/%C3%B3/g, "ó");
            nombre = nombre.replace(/%C3%AD/g, "í");
            nombre = nombre.replace(/%C3%A1/g, "á");
            $scope.sesion.canciones.push({nombre: nombre, fecha: res[i].fecha});
        }

    });

    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };
});

WeRunners.controller('ListasesionesCtrl', function ($scope, $ionicHistory, Sesion, $rootScope, $ionicPlatform, Reproduccion, $firebaseArray) {

    $scope.sesiones = []
    $scope.sesion = {}

    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };

    Sesion.all().then(function(sesion){
        for (i = 0; i < sesion.length; i++) {
                $scope.sesiones.push({"id": sesion[i].id, "fechaInicio": sesion[i].fechaInicio, "fechaFin": sesion[i].fechaFin, "nombre": sesion[i].nombre, "descripcion": sesion[i].descripcion, "key": sesion[i].key, "phone": sesion[i].phone})
            }
    });

    $scope.compartir = function(sesion) {
        $scope.sesion = sesion

      var phonenumber = '';
      while(phonenumber != null && phonenumber.length != 10){
          print(sesion)
        phonenumber  = prompt("Quieres compartir tu evento? (10 digitos)", phonenumber);
      }
      for(var i =0;phonenumber != null && i<$rootScope.firebaseUsers.length;i++){
          if(phonenumber == $rootScope.firebaseUsers[i].phone){
              var otherUserItems = $firebaseArray($rootScope.firebaseUsersRef.child($rootScope.firebaseUsers[i].$id+"/items"));
              otherUserItems.$add({
                   "id": $scope.sesion.id,
                   "fechaInicio": sesion.fechaInicio,
                   "fechaFin": sesion.fechaFin,
                   "nombre": sesion.nombre,
                   "descripcion": sesion.descripcion,
                   "key": sesion.key,
                   "phone": sesion.phone
              });
          }
      }
    }


});

WeRunners.controller('DetailsCtrl', function ($scope, $ionicHistory, Sesion, $rootScope, $ionicPlatform, Reproduccion, $firebaseArray, $stateParams, $cordovaNetwork, $cordovaContacts) {

    $scope.sesion = {
        id: $stateParams.SesionID,
        fechaInicio: null,
        fechaFin: null,
        nombre: null,
        descripcion: null,
        canciones: [],
        phone: $rootScope.user.phone
    }


    Sesion.getSesion($stateParams.SesionID).then(function(res) {
        for(var i = 0; i < res.length; i ++) {
            $scope.sesion.fechaInicio = res[i].fechaInicio;
            $scope.sesion.fechaFin = res[i].fechaFin;
            $scope.sesion.nombre = res[i].nombre;
            $scope.sesion.descripcion = res[i].descripcion;
        }
    });

    Reproduccion.getById($stateParams.SesionID).then(function(res) {
        for(var i = 0; i < res.length; i ++) {
            temp = res[i].cancion.split("/");
            nombre = temp[temp.length - 1]
            nombre = nombre.replace(/%20/g, " ");
            nombre = nombre.replace(/%C3%B3/g, "ó");
            nombre = nombre.replace(/%C3%AD/g, "í");
            nombre = nombre.replace(/%C3%A1/g, "á");
            $scope.sesion.canciones.push({nombre: nombre, fecha: res[i].fecha});
        }

    });

    $scope.myGoBack = function() {
        $ionicHistory.goBack();
    };

    $scope.compartir = function(sesion) {
        $scope.sesion = sesion

        console.log($cordovaNetwork.isOnline());
        if($cordovaNetwork.isOnline() == false) {
            alert("No hay conexión en el momento, por favor, intente más tarde.")
            $ionicHistory.goBack();
        } else {
            /*
                 $cordovaContacts.pickContact().then(function (contactPicked) {
                     console.log(JSON.stringify(contactPicked.phoneNumbers.value))

                     var phonenumber = contactPicked.phoneNumbers.value;
                     //console.log(JSON.stringify(contactPicked.phoneNumbers))                     console.log(JSON.stringify(contactPicked.phoneNumbers.value))
                     console.log(JSON.stringify(contactPicked.phoneNumbers))
                  console.log(JSON.stringify(contactPicked.phoneNumbers[0].value))



                     phonenumber = contactPicked.phoneNumbers[0].value
                     phonenumber = phonenumber.replace(" ", "");
                     console.log(phonenumber)
                     for(var i =0;phonenumber != null && i<$rootScope.firebaseUsers.length;i++){
                          if(phonenumber == $rootScope.firebaseUsers[i].phone){
                              var otherUserItems = $firebaseArray($rootScope.firebaseUsersRef.child($rootScope.firebaseUsers[i].$id+"/items"));
                              otherUserItems.$add({
                                   "id": $scope.sesion.id,
                                   "fechaInicio": sesion.fechaInicio,
                                   "fechaFin": sesion.fechaFin,
                                   "nombre": sesion.nombre,
                                   "descripcion": sesion.descripcion,
                                   "key": sesion.key,
                                   "phone": sesion.phone
                              });
                          }
                      }


                });

                $cordovaContacts.find({filter: ''}).then(function(result) {
                    for(var i = 0; i < result.length; i ++ ) {
                        console.log(JSON.stringify(result[i]))
                    }
                    $scope.contacts = result;
                }, function(error) {
                    console.log("ERROR: " + error);
                });

            */
            Sesion.getSesion($stateParams.SesionID).then(function(res) {
                for(var i = 0; i < res.length; i ++) {
                    $scope.sesion.fechaInicio = res[i].fechaInicio;
                    $scope.sesion.fechaFin = res[i].fechaFin;
                    $scope.sesion.nombre = res[i].nombre;
                    $scope.sesion.descripcion = res[i].descripcion;
                }
                var phonenumber = '';
              while(phonenumber != null && phonenumber.length != 10){
                  print(sesion)
                phonenumber  = prompt("Quieres compartir tu evento? (10 digitos)", phonenumber);
              }
              for(var i =0;phonenumber != null && i<$rootScope.firebaseUsers.length;i++){
                  if(phonenumber == $rootScope.firebaseUsers[i].phone){
                      var otherUserItems = $firebaseArray($rootScope.firebaseUsersRef.child($rootScope.firebaseUsers[i].$id+"/items"));
                      console.log(JSON.stringify($scope.sesion));
                      otherUserItems.$add({
                           "id": $scope.sesion.id,
                           "fechaInicio": $scope.sesion.fechaInicio,
                           "fechaFin": $scope.sesion.fechaFin,
                           "nombre": $scope.sesion.nombre,
                           "descripcion": $scope.sesion.descripcion,
                           /*"key": sesion.key,*/
                           "phone": $scope.sesion.phone
                      });
                  }
              }
            });

        }
    };
});

WeRunners.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/home')

  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl'
  })

  $stateProvider.state('eventos', {
    url: '/eventos',
    templateUrl: 'templates/eventos.html',
    controller: 'eventosCtrl'
  })

  $stateProvider.state('configuration', {
    url: '/configuration',
    templateUrl: 'templates/configuration.html',
    controller: 'configurationCtrl'
  })

  $stateProvider.state('sensibilidad', {
    url: '/sensibilidad',
    templateUrl: 'templates/sensibilidad.html',
    controller: 'sensibilidadCtrl'
  })

  $stateProvider.state('busquedaCanciones', {
    url: '/busquedaCanciones',
    templateUrl: 'templates/busquedaCanciones.html',
    controller: 'busquedaCancionsCtrl'
  })

  $stateProvider.state('factorMultiplicacion', {
    url: '/factorMultiplicacion',
    templateUrl: 'templates/factorMultiplicacion.html',
    controller: 'multiplicacionCtrl'
  })

  $stateProvider.state('reproductor', {
    url: '/reproductor',
    templateUrl: 'templates/reproductor.html',
    controller: 'reproductorCtrl'
  })

  $stateProvider.state('detalleSesion', {
    url: '/detalleSesion',
    templateUrl: 'templates/detalleSesion.html',
    controller: 'detalleSesionCtrl'
  })

  $stateProvider.state('sesiones', {
    url: '/Listasesiones',
    templateUrl: 'templates/Listasesiones.html',
    controller: 'ListasesionesCtrl'
  })

  $stateProvider.state('details', {
    url: '/details/:SesionID',
    templateUrl: 'templates/details.html',
    controller: 'DetailsCtrl'
  })



})

WeRunners.factory("$fileFactory", function($q) {
    var File = function() { };

    File.prototype = {

        getParentDirectory: function(path) {
            var deferred = $q.defer();
            window.resolveLocalFileSystemURL(path, function(fileSystem) {
                fileSystem.getParent(function(result) {
                    deferred.resolve(result);
                }, function(error) {
                    deferred.reject(error);
                });
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        },

        getEntriesAtRoot: function() {
            var deferred = $q.defer();
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                var directoryReader = fileSystem.root.createReader();
                directoryReader.readEntries(function(entries) {
                    deferred.resolve(entries);
                }, function(error) {
                    deferred.reject(error);
                });
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        },

        getEntries: function(path) {
            var deferred = $q.defer();
            window.resolveLocalFileSystemURL(path, function(fileSystem) {
                var directoryReader = fileSystem.createReader();
                directoryReader.readEntries(function(entries) {
                    deferred.resolve(entries);
                }, function(error) {
                    deferred.reject(error);
                });
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

    };

    return File;

});

WeRunners.factory('DBA', function($cordovaSQLite, $q, $ionicPlatform) {
  var self = this;

  // Handle query's and potential errors
  self.query = function (query, parameters) {
    parameters = parameters || [];
    var q = $q.defer();

    $ionicPlatform.ready(function () {
      $cordovaSQLite.execute(db, query, parameters)
        .then(function (result) {
          q.resolve(result);
        }, function (error) {
          alert("2")
          alert(JSON.stringify(error))
          q.reject(error);
        });
    });
    return q.promise;
  }

  // Proces a result set
  self.getAll = function(result) {
    var output = [];

    for (var i = 0; i < result.rows.length; i++) {
      output.push(result.rows.item(i));
    }
    return output;
  }

  // Proces a single result
  self.getById = function(result) {
    var output = null;
    output = angular.copy(result.rows.item(0));
    return output;
  }

  return self;
});

// Operaciones sobre la tabla Cancion
WeRunners.factory('Cancion', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT ruta, bpm FROM cancion")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.buscarPorBPMMayor = function(bpm) {
      var parameters = [bpm]
      return DBA.query("SELECT * FROM Cancion where bpm < (?) order by bpm desc limit 1", parameters).then(function(result) {
        return DBA.getAll(result);
      });;

  }

  self.buscarPorBPMMenor = function(bpm) {
      var parameters = [bpm]
      return DBA.query("SELECT * FROM Cancion where bpm > (?) order by bpm asc limit 1", parameters).then(function(result) {
        return DBA.getAll(result);
      });;

  }


  self.get = function(memberId) {
    var parameters = [memberId];
    return DBA.query("SELECT id, name FROM team WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(cancion) {
    var parameters = [cancion.ruta, cancion.bpm];
    return DBA.query("INSERT INTO cancion (ruta, bpm) VALUES (?,?)", parameters);
  }

  self.removeAll = function() {
      return DBA.query("DELETE FROM cancion");
  }

  self.remove = function(member) {
    var parameters = [member.id];
    return DBA.query("DELETE FROM team WHERE id = (?)", parameters);
  }

  self.update = function(origMember, editMember) {
    var parameters = [editMember.id, editMember.name, origMember.id];
    return DBA.query("UPDATE team SET id = (?), name = (?) WHERE id = (?)", parameters);
  }

  return self;
})

WeRunners.factory('BPMRun', function($cordovaDeviceMotion, $ionicPlatform, Cancion, $cordovaMedia, Reproduccion, $rootScope) {

    var self = this;

    self.delta_picos = [];
    self.MUESTRAS_ANTES = 50;
    self.ant_time = 0;
    self.vlr_aceleraccion = 0;
    self.media = null;

    self.obtenerValor = function(measurement) {
        dif_X = Math.abs(9.8 - Math.abs(measurement.x));
        dif_Y = Math.abs(9.8 - Math.abs(measurement.y));
        dif_Z = Math.abs(9.8 - Math.abs(measurement.z));

        if(dif_X <= dif_Y && dif_X <= dif_Z) {
            return Math.abs(measurement.x);
        }
        else if(dif_Y <= dif_X && dif_Y <= dif_Z) {
            return Math.abs(measurement.y);
        }
        else {
            return Math.abs(measurement.z);
        }

    }

    self.registrar = function(measurement) {
        valor = self.obtenerValor(measurement);

        diferencia = measurement.timestamp - self.ant_time;

        if(valor >= ((self.vlr_aceleraccion * (window.localStorage['sensibilidad'] || 0))/100)) {
            self.vlr_aceleraccion = valor;
        } else {
            if(self.ant_time == 0) {
                self.ant_time = measurement.timestamp;
            } else {
                if(self.delta_picos.length < self.MUESTRAS_ANTES) {
                    self.delta_picos.push(diferencia);
                    self.ant_time = measurement.timestamp;
                    //bpm_cancion = self.calcularBPM(self.delta_picos);
                    //self.buscarReproducir(bpm_cancion);
                    // CalcularBPM
                } else if(self.delta_picos.length > self.MUESTRAS_ANTES) {
                    self.delta_picos.splice(0,1);
                    self.delta_picos.push(diferencia);
                    self.ant_time = measurement.timestamp;
                    bpm_cancion = self.calcularBPM(self.delta_picos);
                    //console.log("Se busca canción a reproducir con BPM " + bpm_cancion)
                    //self.buscarReproducir(bpm_cancion);
                    //Calcular BPM
                } else {
                    self.delta_picos.push(diferencia);
                    self.ant_time = measurement.timestamp;
                    // Reproducir canción
                    bpm_cancion = self.calcularBPM(self.delta_picos);
                    console.log("1Se busca canción a reproducir con BPM " + bpm_cancion)
                    self.buscarReproducir(bpm_cancion);


                }
            }
        }
    }

    self.calcularBPM = function(array) {
        sum = 0;
        for (var i in array) {
            sum = sum + array[i];
        }

        average = sum / array.length;
        return parseInt(60000 / average);

    }

    self.terminarSesion = function() {
        if(JSON.stringify(self.media) != "null") {
            self.media.stop();
        }


    }

    self.buscarReproducir = function(bpm) {
        bpmMayor = 0;
        bpmMenor = 0;

        Cancion.buscarPorBPMMayor(bpm).then(function(team){
            for (i = 0; i < team.length; i++) {
                    bpmMayor = team[i].bpm

                    if ('null' != JSON.stringify(self.media)) {
                        self.media.stop();
                    }
                    self.media = $cordovaMedia.newMedia(team[i].ruta);
                    self.media.play(); // Android
                    Reproduccion.add($rootScope.idSesion, team[i].ruta);

                }
        });

        /*Cancion.buscarPorBPMMenor(bpm).then(function(team){
            for (i = 0; i < team.length; i++) {
                    //temp = team[i].ruta.split("/");
                    console.log(JSON.stringify(team[i]))
                    bpmMenor = team[i].bpm
                    //$scope.canciones.push({"nombre": nombre, "bpm": team[i].bpm, "ruta": team[i].ruta})

                }
        });

        console.log(bpmMayor)
        console.log(bpmMenor)   */



    }

    return self;
})

var num = 1;
WeRunners.factory("Items", function($firebaseArray,$rootScope,$ionicPlatform,$state, Sesion) {

  return {
      run: function(){
        $rootScope.firebaseRef = new Firebase("https://werunners.firebaseio.com/users/"+$rootScope.user.userId+"/items");

        $ionicPlatform.ready(function() {
            $rootScope.firebaseRef.on('child_added', function(childSnapshot, prevChildKey) {
              var item = childSnapshot.val();
              item.key = childSnapshot.key();
              // code to handle new child.

              //TODO hacerlo con los datos reales
              var eventsSQLite = [];
              Sesion.getKeys(item).then(function(returnObj){
                  var result = returnObj.result;
                  var citem = returnObj.item;

                  for(var i =0;i<result.length;i++) {
                      eventsSQLite.push({key: result[i].key})
                  }
                  var that = this;
                  var old = false;
                  for(var i =0;i<eventsSQLite.length;i++){
                    if(eventsSQLite[i].key == citem.key){
                        old = true;
                    }
                  }
                  if(!old && citem.phone != $rootScope.user.phone){
                    //TODO registrarlo en el SQLite el key es: childSnapshot.key()

                      // Guardar en la base de datos local
                      Sesion.get(citem.id).then(function(result) {


                      })



                      Sesion.get(citem.id).then(function(result) {
                        //if(result[0])
                        if(result.length == 0) {
                            Sesion.add(citem);
                        } else {
                            Sesion.update(citem)
                        }

          })

                    //alert(item.name+" te ha compartido su evento");
                    window.cordova.plugins.notification.local.schedule({
                        title:"WeRunners",
                        message: citem.phone + " te ha compartido su evento",

                        // item.phone en vez de name, y se refiere al que creo el evento

                        //message: "Hi, are you ready? We are waiting.",
                        //sound: "file://sounds/message.mp3",
                        //icon: "http://my.domain.de/avatar/user#id=123"
                    });
                    cordova.plugins.notification.local.on("click", function (notification) {
                        $state.go('sesiones');
                    });
                    console.error("Te han compartido un evento");
                  }
                  num++;
                });
            });
            $rootScope.firebaseRef.on('child_changed', function(childSnapshot, prevChildKey) {
              // code to handle child data changes.
              console.log("Changed");
              console.log(JSON.stringify(childSnapshot.val())+" + "+prevChildKey);
            });

        })
        return $firebaseArray($rootScope.firebaseRef);
      }

  };

})

// Operaciones sobre la tabla Cancion
WeRunners.factory('Sesion', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT id, fechaInicio, fechaFin, nombre, descripcion, key, phone FROM sesion")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.maxID = function() {
    return DBA.query("SELECT max(id) as max FROM sesion")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.crear = function(phone) {
    resultado = (self.maxID()).then(function(result){
    for (i = 0; i < result.length; i++) {
        if(JSON.stringify(result[i].max) == "null") {
            id =  device.uuid + "KOKO1";
            var f=new Date();
            actual= f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear() + " " + f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();

            parameters = [id, actual, phone]
            DBA.query("INSERT INTO sesion (id, fechaInicio, phone) VALUES (?,?,?)", parameters);
            return id;
        } else {

            arreglo = result[i].max.split("KOKO");
            numero = arreglo[1];
            numero = parseInt(numero) + 1
            id = device.uuid + "KOKO" + numero

            var f=new Date();
            actual= f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear() + " " + f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();

            parameters = [id, actual, phone]
            DBA.query("INSERT INTO sesion (id, fechaInicio, phone) VALUES (?,?,?)", parameters);
            return id;
        }
    }
    });

    return resultado;
  }

  self.cerrar = function(id) {
    var f=new Date();
    actual= f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear() + " " + f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();

    var parameters = [actual, id];
    return DBA.query("UPDATE sesion SET fechaFin = (?) WHERE id = (?)", parameters);
  }

  self.get = function(Id) {
    var parameters = [Id];
    resultado = DBA.query("SELECT fechaInicio, fechaFin FROM sesion WHERE id = (?)", parameters).then(function(result) {
        return DBA.getAll(result);
      });
    return resultado;
  }

  self.getSesion = function(Id) {

        var parameters = [Id];
        resultado = DBA.query("SELECT id, fechaInicio, fechaFin, nombre, descripcion, key, phone FROM sesion WHERE id = (?)", parameters).then(function(result) {
        return DBA.getAll(result);
        });
    return resultado;
  }

  self.update = function(item) {
    var parameters = [item.key, item.id];
    return DBA.query("UPDATE sesion SET key = (?) WHERE id = (?)", parameters);
  }

  self.guardar = function(id, nombre, descripcion) {

    var parameters = [nombre, descripcion, id];
    return DBA.query("UPDATE sesion SET nombre = (?), descripcion = (?) WHERE id = (?)", parameters);
  }

  self.add = function(sesion) {
      parameters = [sesion.id, sesion.fechaInicio, sesion.fechaFin, sesion.nombre, sesion.descripcion, sesion.key, sesion.phone]
      DBA.query("INSERT INTO sesion (id, fechaInicio, fechaFin, nombre, descripcion, key, phone) VALUES (?,?,?,?,?,?,?) ", parameters);

  }

  self.getKeys = function(pitem) {
      return DBA.query("SELECT key FROM sesion")
            .then(function(presult){
              return { result: DBA.getAll(presult), item:pitem}
            });


  }

  self.removeAll = function() {
      return DBA.query("DELETE FROM cancion");
  }

  self.remove = function(member) {
    var parameters = [member.id];
    return DBA.query("DELETE FROM team WHERE id = (?)", parameters);
  }

  self.update = function(origMember, editMember) {
    var parameters = [editMember.id, editMember.name, origMember.id];
    return DBA.query("UPDATE team SET id = (?), name = (?) WHERE id = (?)", parameters);
  }

  return self;
})

// Operaciones sobre la tabla Cancion
WeRunners.factory('Reproduccion', function($cordovaSQLite, DBA) {
  var self = this;

  self.getById = function(id) {
      parameters = [id]
    return DBA.query("SELECT cancion, fecha FROM reproduccion WHERE id = (?)", parameters)
      .then(function(result){
        return DBA.getAll(result);
      });
  }



  self.add = function(id, cancion) {
        var f=new Date();
        actual= f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear() + " " + f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();

        parameters = [id, cancion, actual]
        return DBA.query("INSERT INTO reproduccion (id, cancion, fecha) VALUES (?,?,?)", parameters);

    }






  return self;
})
